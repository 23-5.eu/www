+++
title = "Weather prediction"
date = "2010-04-09"
image = 'read.jpg'
+++

# Weather

## Motionlab Station
 - [Static Live Weather](https://weather.23-5.eu/)
 - ![Temperature](https://weather.23-5.eu/daytempdew.png)
 - ![Rain](https://weather.23-5.eu/dayrain.png)
 - [Personal Weather Station](https://www.wunderground.com/dashboard/pws/IBERLIN1705)
 - [Allsky](https://allsky.23-5.eu)
 - ![Live Cam](https://allsky.23-5.eu/tmp/image.jpg)

## Links to live data
 - ![DVD Berlin Radar](https://www.dwd.de/DWD/wetter/radar/radfilm_bbb_akt.gif)
 - [Blitzortung](https://map.blitzortung.org/#7/52.4938152/13.4481841)
 - [WunderMap](https://www.wunderground.com/wundermap)


# Prediction

## Self calculated for Motionlab
 - [Prediction](https://prediction.23-5.eu)

## Usefull links
 - [14 days from Meteoblue](https://www.meteoblue.com/en/weather/14-days/berlin_germany_2950159)
 - [5 days from Kachelmann](https://kachelmannwetter.com/de/vorhersage/2950159-berlin/kompakt/deu-mos)
 - [Windy](https://www.windy.com/-Weather-radar-radar?radar,52.4938152,13.4481841,7)
 - [Nasa Fire Map](https://firms.modaps.eosdis.nasa.gov/map/#d:24hrs;l:noaa20-viirs,viirs,modis_a,modis_t,street;@13.4481841,52.4938152,9z)